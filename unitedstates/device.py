import json
import os
import queue
import re
import threading
import time

import tango
from tango.server import Device, attribute, command, device_property

from .child import ChildDevice, CompoundChildDevice
from .ttldict import TTLDict


def format_error(error):
    return {
        "reason": str(error.reason),
        "severity": str(error.severity),
        "desc": str(error.desc),
        "origin": str(str),
    }


class UnitedStates(Device):

    """
    A device whose purpose is to monitor the State of other devices.
    It is configured with a list of device names, each of which will
    be subscribed to for changes in state. It can also be given other
    UnitedStates devices, from which it will then collate ChildData
    into its own.
    """

    Devices = device_property(dtype=[str], doc="List of devices to monitor State of")

    MinimumUpdatePeriod = device_property(
        dtype=float, default_value=0.5, doc="Shortest time (s) between sending events"
    )

    def init_device(self):
        self.get_device_properties()

        self.children = {}

        self.set_change_event("ChildData", True)
        self.get_device_attr().get_attr_by_name("childData").set_data_ready_event(True)

        self.last_update = 0  # keep the time of the latest change
        self.version = 0  # this number is simply bumped at each change
        self.states = {}  # keeps all the state information about children
        self.bad_children = set()
        self.slow_children = set()
        self.versioned_update_lock = threading.Lock()
        self.updates = TTLDict(60)  # keep updates for 1 minute
        self.update_queue = queue.Queue()
        self.publisher_done = threading.Event()
        self.publisher = threading.Thread(
            target=self.publisher_thread, name="publisher"
        )
        self.publisher.daemon = True
        self.publisher.start()

        # do the setup in a thread so that the device is available
        self.timer = threading.Timer(1, self.start).start()

    def delete_device(self):
        try:
            self.info_stream("Stopping publisher thread")
            self.publisher_done.set()
            self.enqueue_update(device="", updates={}, full=False)
            self.publisher.join()
            self.publisher = None
            self.info_stream("Stopped publisher thread")

            for device, child in self.children.items():
                try:
                    child.cleanup()
                except tango.DevFailed as e:
                    # This is probably OK...
                    self.debug_stream("Could not unsubscribe from %s: %s" % (device, e))
            self.children.clear()
        except Exception as e:
            self.debug_stream(str(e))

    def start(self):
        "Set up all child device listeners"
        self.set_state(tango.DevState.INIT)
        self.setup_children()
        time.sleep(1)
        self.set_state(tango.DevState.ON)
        time.sleep(1.0)
        self.enqueue_update(device="", updates={}, full=True)

    def setup_children(self):
        "Set up listeners for all child devices."
        self._devices = set(sum([self.expand_device(dev) for dev in self.Devices], []))

        # here we will loop until all the devices are subscribed to,
        # waiting around for any that aren't yet "ready". This only
        # applies to other UnitedStates devices that are still in INIT
        # (i.e. still working on setting up their own subcriptions)
        # Normal devices that can't be subscribed to are just skipped.
        devices_left = self.slow_children = set(self._devices)
        while devices_left:
            for device in list(devices_left):
                time.sleep(0.05)
                if device in self.children:
                    child = self.children[device]
                else:
                    child = self.setup_child(device)
                if child and child.is_ready():
                    # don't start subscribing until the child is "ready"
                    try:
                        child.subscribe()
                    except tango.DevFailed as e:
                        self.error_stream("Could not subscribe to %s: %r", device, e)
                        self.bad_children.add(device)
                    devices_left.remove(device)
                    self.debug_stream(
                        "Listening to %s (%d left of %d)"
                        % (device, len(devices_left), len(self._devices))
                    )
                elif child is None:
                    self.bad_children.add(device)
                    devices_left.remove(device)
                    self.error_stream(
                        "Could not create listener for %s. Ignoring.", device
                    )
            if devices_left:
                self.debug_stream("Still waiting for %d devices..." % len(devices_left))
                time.sleep(5.0)

    def setup_child(self, device):
        "Setup connection to a device"
        device = device.upper()
        try:
            if self.is_united_states_device(device):
                child = CompoundChildDevice(
                    device,
                    self.states,
                    self.enqueue_update,
                    self.get_logger(),
                    self.MinimumUpdatePeriod,
                )
            else:
                child = ChildDevice(
                    device, self.states, self.enqueue_update, self.get_logger()
                )
            self.children[device] = child
            return child
        except tango.DevFailed as e:
            self.error_stream("Could not create child device %s: %s" % (device, e))

    def is_united_states_device(self, device):
        tango_host, short_device_name = _split_tango_host_and_device(device)
        db = _get_db_for_tango_host(tango_host)
        info = db.get_device_info(short_device_name)
        is_same_type = info.class_name == self.__class__.__name__
        return is_same_type

    def expand_device(self, device):

        """Takes a device name with optional wildcards and returns the
        list of matching devices from the database."""

        if "*" not in device:  # not a pattern
            return [device.upper()]

        # look up the pattern in the DB
        tango_host, short_device_name = _split_tango_host_and_device(device)
        db = _get_db_for_tango_host(tango_host)
        result = db.get_device_exported(short_device_name)
        prefix = "tango://{}/".format(tango_host) if tango_host else ""

        return [
            "{}{}".format(prefix, d).upper()
            for d in result
            if (
                not str(d).lower().startswith("dserver/")
                and
                # protection against indefinite recursion
                str(d).upper() != self.get_name().upper()
            )
        ]

    def enqueue_update(self, device, updates, full=False):
        self.update_queue.put((device, updates, full))

    def publisher_thread(self):
        def handle_update_in_publisher_thread(device, updates, full):
            """Take a bunch of updates and apply them
            An "update" is just a dict containing one or more devices
            and their new state information."""
            if updates:
                for device_updated, data in updates.items():
                    old_data = self.states.get(device_updated)
                    if not old_data:
                        # No old data means it's not a state change
                        continue
                    self.info_stream(
                        "STATE CHANGE %s: %s -> %s at %f, %d errors (v %d)"
                        % (
                            device_updated,
                            old_data.get("state", "NA"),
                            data["state"],
                            data["last_update"],
                            data.get("errors", 0),
                            self.version,
                        )
                    )
                with self.versioned_update_lock:
                    self.version += 1
                    self.updates[self.version] = updates
                    self.states.update(updates)
                    self.last_update = time.time()
                self.debug_stream(
                    "Got update from %s (%d states, %r, version %d)"
                    % (device, len(updates), list(updates.keys())[:5], self.version)
                )
            if updates or full:
                push_data_changes_in_publisher_thread(full=full)

        def push_data_changes_in_publisher_thread(full):
            "Send out appropriate change events"
            if self.get_state() != tango.DevState.ON:
                return
            if full:
                self.push_change_event("ChildData", self.get_child_data_dump())
            else:
                self.push_data_ready_event("ChildData", self.version)

        with tango.EnsureOmniThread():
            self.info_stream("Starting publisher thread")
            while not self.publisher_done.is_set():
                try:
                    device_, updates_, full_ = self.update_queue.get()
                    handle_update_in_publisher_thread(device_, updates_, full_)
                except Exception as exc:
                    self.error_stream("General exception in publisher thread: %s" % exc)
                    time.sleep(1)

    def get_child_data_dump(self):
        with self.versioned_update_lock:
            data = {
                "last_update": self.last_update,
                "state": str(self.get_state()),
                "version": self.version,
                "devices": self.states,
            }
            return json.dumps(data)

    @attribute(dtype=str)
    def ChildData(self):
        """The full set of current child states, as a JSON string."""
        # looks like we need locking to prevent states from
        # being changed while we serialize
        return self.get_child_data_dump()

    def is_ChildData_allowed(self, attr):
        return self.get_state() == tango.DevState.ON

    @attribute(dtype=int)
    def QueueSize(self):
        """Estimated update queue length."""
        return self.update_queue.qsize()

    @attribute(dtype=int)
    def VersionCount(self):
        """Version count (number of state changes since device startup)."""
        return self.version

    @command(
        dtype_in=[int],
        doc_in="two version numbers",
        dtype_out=str,
        doc_out="JSON encoded updates",
    )
    def GetUpdates(self, interval):
        """Get a JSON object consisting of the states that have changed
        between the two given versions.

        Reading self.updates is not protected with a lock here
        since each value stored is effectively immutable.  New values
        are added in the publisher thread, but the content of each value
        is never modified.  This ensures data consistency for the caller."""

        t0 = time.time()

        if self.get_state() != tango.DevState.ON:
            return "{}"
        start, end = interval
        available = list(self.updates.keys())
        if not available:
            return "{}"

        if min(available) > (start + 1):
            msg = (
                "GetUpdates: Trying to get changes from before the oldest "
                "available version (%d)" % min(available)
            )
            self.error_stream(msg)
            tango.Except.throw_exception(
                "DATA_OUT_OF_RANGE", msg, "", tango.ErrSeverity.ERR
            )
        if max(available) < end:
            msg = "End of interval (%d) higher than current version (%d)" % (
                end,
                self.version,
            )
            self.error_stream(msg)
            tango.Except.throw_exception(
                "DATA_OUT_OF_RANGE", msg, "", tango.ErrSeverity.ERR
            )
        try:
            updates = {}
            # we take all updates from the version after the start (start is assumed
            # to be the version the client has), up until and including the end version
            # (assumed to be the version the client wants to be at)
            for version in range(start + 1, end + 1):
                update = self.updates[version]
                updates.update(update)  # :)
            result = json.dumps(updates)
            self.debug_stream(
                "GetUpdates: %r, %d took %f s", interval, len(updates), time.time() - t0
            )
            return result
        except KeyError as e:
            msg = "GetUpdates: Invalid range [%d, %d] (%d, %d): %s" % (
                start,
                end,
                min(available),
                max(available),
                e,
            )
            self.error_stream(msg)
            tango.Except.throw_exception(
                "DATA_OUT_OF_RANGE", msg, str(e), tango.ErrSeverity.ERR
            )

    @command(dtype_out=[str])
    def BadDevices(self):
        "Returns all child devices that we could not subscribe to"
        if self.get_state() == tango.DevState.ON:
            return sorted(self.bad_children)
        return []

    @command(dtype_out=[str])
    def ErrorsByDevice(self):
        if self.get_state() == tango.DevState.ON:
            return [
                "%s\t%d" % (name, child.errors) for name, child in self.children.items()
            ]
        return []

    @command(dtype_in=[str], dtype_out=[str])
    def GetFilteredDevices(self, fltr):
        "Returns the States for all devices containing the given regex"
        if len(fltr) == 2:
            regex, desired_state = [re.compile(f, re.IGNORECASE) for f in fltr]
        else:
            regex, desired_state = [
                re.compile(f, re.IGNORECASE) for f in (fltr[0], ".*")
            ]

        result = []
        row = "%s\t%s\t%s"
        for device in sorted(self.states):
            data = self.states[device.upper()]
            state = data["state"]
            if regex.search(device) and desired_state.match(state):
                last_update = data["last_update"]
                result.append(row % (device, state, last_update))

        return result

    def dev_status(self):
        state = self.get_state()
        self._status = "Device is in %s state.\n" % self.get_state()
        if state == tango.DevState.INIT:
            self._status += "%d of %d devices subscribed.\n" % (
                len(self.children),
                len(self._devices),
            )
        elif state == tango.DevState.ON:
            self._status += "Receiving states from %d devices.\n" % len(self.states)
        if self.bad_children:
            self._status += "%d devices could not be reached.\n" % len(
                self.bad_children
            )
        if self.slow_children:
            if len(self.slow_children) > 3:
                self._status += "Waiting for %d devices..." % len(self.slow_children)
            else:
                self._status += "Waiting for %s..." % ", ".join(self.slow_children)
        self.set_status(self._status)
        return self._status


def _get_db_for_tango_host(tango_host):
    """Return correct Database instance, even for a different TANGO_HOST."""
    old_tango_host = os.getenv("TANGO_HOST")
    try:
        if tango_host is not None:
            os.environ["TANGO_HOST"] = tango_host
        return tango.Database()
    finally:
        if old_tango_host is not None:
            os.environ["TANGO_HOST"] = old_tango_host
        elif "TANGO_HOST" in os.environ:
            del os.environ["TANGO_HOST"]


def _split_tango_host_and_device(device):
    """Return TANGO_HOST and short device name"""
    match = re.match(
        r"^tango://(?P<tango_host>[a-z0-9.\-]+?:\d+)/(?P<device>.*)", device.lower()
    )
    if match:
        tango_host = match.group("tango_host")
        short_device_name = match.group("device")
    else:
        tango_host = None
        short_device_name = device
    return tango_host, short_device_name


def main():
    UnitedStates.run_server()


if __name__ == "__main__":
    main()
