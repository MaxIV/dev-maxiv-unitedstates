#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="tangods-unitedstates",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    author="KITS SW",
    author_email="kits-sw@maxiv.lu.se",
    license="GPLv3",
    url="https://gitlab.maxiv.lu.se/kits-maxiv/dev-maxiv-unitedstates",
    description="Device server that collects the State of other devices.",
    packages=find_packages(),
    entry_points={"console_scripts": ["UnitedStates = unitedstates.device:main"]},
    install_requires=["pytango"],
    extras_require={"tests": ["pytest", "dsconfig"]},
)
